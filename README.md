# SMC demos

More details in this [page](http://www-sop.inria.fr/members/Fabien.Campillo/software/ibm-forest/index.html).

## Description
An Individual-based model simulator for forest dynamics including a zone of influence mark developed in 2008 and 2009 [PDF](http://www-sop.inria.fr/members/Fabien.Campillo/wp-content/uploads/2022/12/campillo2009c.pdf).

## Visuals
See this [page](http://www-sop.inria.fr/members/Fabien.Campillo/software/ibm-forest/index.html).


## Usage

Read `LISEZMOI.txt` in `matlab` directory.


## Authors and acknowledgment



## License
[License](License.md)


## Project status
SMC demos is written with an ''old'' matlab, an update in a recent matlab would be welcome. 



## AUTHOR

Conception: 

- [Fabien Campillo](http://www-sop.inria.fr/members/Fabien.Campillo/index.html)
- Marc Joannides 


Authors:
 
- [Fabien Campillo](http://www-sop.inria.fr/members/Fabien.Campillo/index.html)
- [Nicolas Desassis](https://www.minesparis.psl.eu/Services/Annuaire/nicolas-desassis) 

 
[Fabien Campillo](http://www-sop.inria.fr/members/Fabien.Campillo/index.html), [MathNeuro Team](https://team.inria.fr/mathneuro/), [Inria centre
at Université Côte d’Azur](https://www.inria.fr/en/inria-centre-universite-cote-azur)
Fabien.Campillo@inria.fr
