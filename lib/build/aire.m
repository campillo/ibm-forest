function aire=aire(d,r,R)
%fonction permettant de calculer l'aire entre deux ensembles 			
%   de cercles
%	           Arguments :
%		   d : un vecteur des distances séparant les deux ensembles de 	
%		       cercles
%		   r : un vecteur contenant les rayons du premier ensemble de 	
%		       cercles
%		   R : un vecteur contenant les rayons du second ensemble de 	
%		       cercles
%		   Les longueurs de ces trois vecteurs doivent �tre identiques.
% 		   Sortie : le vecteur des aires des intersections deux � deux 	
%			    des cercles.
r2   = r.*r;
R2   = R.*R;
x    = (r2-R2+d.*d)./(2*d);
rx   = x./r;
Rx   = (d-x)./R;
aire = acos(sign(rx).*min(abs(rx),1))...
    .*r2+acos(sign(Rx).*min(abs(Rx),1))...
    .*R2-(r+R).*sqrt(max(r2-x.*x,0));
