function dist=distance(x,X,dxM,dyM,Size)
%fonction permettant de calculer les distances s�parant un point �
% un ensemble de points sur un tore.
%		   Arguments :
%		   x   : valeur de l'abscisse du point
%		   X   : vecteur des abscisses des points de l'ensemble
%		   y   : valeur de l'ordonnée du point
%		   Y   : vecteur des ordonnées des points de l'ensemble
%		   dxM : �tendue du tore en abscisse
%		   dyM : �tendue du tore en ordonn�e
%		   Size : nombre de points dans l'ensemble de points
%		   Sortie :
%	           le vecteur des distances entre le point et les points de l'ensemble
tempx = x(1)*ones(Size,1);
tempy = x(2)*ones(Size,1);
ecx   = abs(tempx-X(1:Size,1));
ecy   = abs(tempy-X(1:Size,2));
Cx    = dxM*ones(Size,1);
Cy    = dyM*ones(Size,1);
dx    = min([ecx Cx-ecx],[],2);
dy    = min([ecy Cy-ecy],[],2);
dist  = sqrt(dx.*dx+dy.*dy);
