function dist=distance2(Foret,dxM,dyM,Size)
tempx = repmat(Foret(:,1),1,Size);
tempy = repmat(Foret(:,2),1,Size);
ecx   = abs(tempx-tempx');
ecy   = abs(tempy-tempy');
Cx    = dxM*ones(Size);
Cy    = dyM*ones(Size);
dx    = min(cat(3,ecx,Cx-ecx),[],3);
dy    = min(cat(3,ecy,Cy-ecy),[],3);
dist  = sqrt(dx.*dx+dy.*dy);

