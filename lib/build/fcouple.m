function res=fcouple(p,q,P,Q)
%--------------------------------------------------------------------------
%fonction permettant de renvoyer un entier unique �partir de deux 
% entiers, chacun �tant compris dans une gamme d'entiers.
% 		   Arguments :
%		   p : premier entier
%	           q : second entier
%		   P : entier maximum de la gamme pour le premier entier
%	           Q : entier maximum de la gamme pour le second entier
%		   Sortie : un entier entre 1 et P*Q
%--------------------------------------------------------------------------
res=P*mod(q-1,Q)+mod(p-1,P)+1;
