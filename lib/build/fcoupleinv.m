function res=fcoupleinv(y,P,Q)
%--------------------------------------------------------------------------
% fonction inverse de fcouple, elle renvoie deux entiers, chacun dans une 
% gamme � partir d'un entier.
% Arguments :
%	y : entier
%   P : gamme du premier entier renvoy�
%	Q : gamme du second entier renvoy�
%--------------------------------------------------------------------------
 q   = floor((y-1)/P)+1;
 p   = y-P*(q-1);
 res = [p' q'];
