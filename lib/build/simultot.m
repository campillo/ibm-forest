function simultot(new,indsav)
%--------------------------------------------------------------------------
% fonction effectuant la simulation et cr�ant les fichiers de sauvegardes.
% Arguments :
%   new     : bool�en indiquant s'il s'agit d'une nouvelle simulation 
%             ou de la suite d'une simulation existante
%   indsave : clef de la simulation
%--------------------------------------------------------------------------


h_wair_bar = waitbar(0,'Please wait...'); % wait bar

%Dossier o� sont rang�s les fichiers de la simulation
chemin     = ['simulforest_' indsav]; 
cheminsave = [chemin '/save/save'];
load([chemin '/paramsim.mat'])

numinstr=size(param,1);

Rm         = param(numinstr,1); 
RM         = param(numinstr,2); 
rc         = param(numinstr,3); 
taillep    = param(numinstr,4); 
gamma      = param(numinstr,5); 
sigdis     = param(numinstr,6); 
mu         = param(numinstr,7); 
alpha      = param(numinstr,8);
Xm         = param(numinstr,9);
XM         = param(numinstr,10);
Ym         = param(numinstr,11);
YM         = param(numinstr,12); 
Size       = param(numinstr,13); 
deltaeuler = param(numinstr,14); 
deltasave  = param(numinstr,15); 
Niter      = param(numinstr,16); 
NombreMax  = param(numinstr,17);
Tps        = param(numinstr,19); 
numfich    = param(numinstr,21);

% For�t initiale

dxM = XM-Xm;
dyM = YM-Ym;

if new
    Foret0 = [rand(Size,1)*(XM-Xm)...
        +Xm rand(Size,1)*(YM-Ym)...
        +Ym rand(Size,1)*(RM-Rm)+Rm];
    Xs = Foret0;
    save([cheminsave  num2str(numfich) '.mat'],'Xs');
else
    load([cheminsave  num2str(numfich) ],'Xs');
    Foret0 = Xs;
    %numfich=numfich-1;
    Size = size(Xs,1);
end

%Chemin pour stocker la matrice d'histoire
ress=[chemin '/save/res' num2str(numinstr) '.mat']; 
    
%%%%% Quantit�s de travail - Facultatif pour le fonctionnement %%%%%%%%%%%%
rand('twister', 5489)
tic;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ind      = 1:Size;              %Indices des arbres en vie
inddispo = (Size+1):NombreMax;  %Infices disponibles


%Quantit�s de travail
NombreMaxmoins = NombreMax-1;
Tpsprochsave   = Tps+deltasave;
Tpsprocheuler  = Tps+deltaeuler;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Cellules partitionnant l'espace

dMax=2*RM; %Distance � partir de laquelle il n'y a plus de comp�tition ressentie.

rx=dxM/(dMax); 
ry=dyM/(dMax);


Nx=floor(rx); %Nombre d'�l�ments pour la partition en absisses
Ny=floor(ry); %Nombre d'�l�ments pour la partition en ordonn�es

Rx=dxM/Nx;    %Dimension des cellules en absisses
Ry=dyM/Ny;    %Dimension des cellules en ordonn�es

Ngrid=Nx*Ny; % Nombre total de cellules


%%%%%%%%%%%%%%%%%%%Place M�moire%%%%%%%%%%%%%%%%%%%%%%%%
indpos = zeros(NombreMax,1);
Cell   = sparse(NombreMax,Ngrid);
Foret  = zeros(NombreMax,3);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%% Quantit�s al�atoires %%%%%%%%%%%%%
Temps     = -log(rand(Niter,1));
evunif    = rand(Niter,1);
evouinon  = rand(Niter,1);
evouinon2 = (RM>taillep)*RM*rand(Niter,1);
ordre1    = ones(Niter,1)+(rand(Niter,1)<1/2);
ordre2    = 3*ones(Niter,1)-ordre1;
Vect      = [sigdis*randn(Niter,2) ones(Niter,1)*Rm];
Rand1     = rand(Niter,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Mres=zeros(Niter,2+size(Foret,2)); % Matrice d'histoire

for j=1:Size
    x        = Foret0(j,1);
    y        = Foret0(j,2);
    nx       = floor((x-Xm)/Rx)+1;
    ny       = floor((y-Ym)/Ry)+1;
    cellcour = fcouple(nx,ny,Nx,Ny);
    Cell(j,cellcour)=j;
end

voisindx = [-1;0;1;-1;0;1;-1;0;1];
voisindy = [-1;-1;-1;0;0;0;1;1;1];

%graphe de voisinage des cellules

Graph = zeros(Ngrid);
temp  = fcoupleinv(1:(Ngrid),Nx,Ny);

for j=1:(Nx*Ny)
    temp2=temp(j*ones(9,1),:)+[voisindx voisindy];
    cellvois=fcouple(temp2(:,1),temp2(:,2),Nx,Ny);
    Graph(cellvois,j)=cellvois;
end


Distance = distance2(Foret0,dxM,dyM,Size);

Foret(ind,:) = Foret0 ;

indpos(ind)=ind;

Vois = Distance<dMax & Distance~=0;


[cand1 cand2] = find(triu(Vois));
cand  = [cand1 cand2];
Nvois = size(cand1,1);

indcoup = sub2ind([Size Size], cand1, cand2);
Dist    = Distance(indcoup);

clear('Vois')

gammam=gamma*RM;

m1 = gammam*Size;   
m2 = mu*Size; 
m3 = alpha*Nvois; 
m0 = m1+m2+m3;  

r = Foret(cand1,3);
R = Foret(cand2,3);
Compet          = sparse(Size,Size);
% Comp�tition deux � deux
Compet(indcoup) = aire(Dist,r,R)./(pi*r.*r); 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                      D�but de la boucle                             %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for j = 1:Niter

    if(round(j/10000)==j/10000)
        waitbar(j/Niter)
    end

    Tps=Tps+Temps(j)/m0;
    
     
    if Tps>min(Tpsprocheuler,Tpsprochsave)
        % Si il y a une sauvegarde � effectuer
        % ou une mise � jour des tailles

        Xs    = Foret(ind,:);
        Ds    = Dist(1:Nvois);
        Cands = cand(1:Nvois,:);

        while Tps>min(Tpsprocheuler,Tpsprochsave)

            if Tps>Tpsprocheuler
                tailles = Xs(:,3);
                i1 = indpos(Cands(:,1));
                i2 = indpos(Cands(:,2));
                % Conversion du type d'indexation
                indcoup1 = sub2ind([Size Size], i1, i2);
                indcoup2 = sub2ind([Size Size], i2, i1);
                r = Xs(i1,3);
                R = Xs(i2,3);
                temp = aire(Ds,r,R);
                Compet(indcoup1) = temp./(pi*r.*r);
                Compet(indcoup2) = temp./(pi*R.*R);
                comp=ones(Size,1)-min(sum(Compet,2),1);
                tailles = tailles+deltaeuler*rc*comp ...
                    .*(ones(Size,1)-tailles./RM).*tailles;
                Xs(:,3) = tailles;
                Tpsprocheuler=Tpsprocheuler+deltaeuler;
            end
            if Tps>Tpsprochsave
                numfich=numfich+1;
                save([cheminsave  num2str(numfich) '.mat'],'Xs');
                param(numinstr,19) = Tpsprochsave;
                param(numinstr,21) = numfich;
                save([chemin '/paramsim.mat'],'param');
                Tpsprochsave=Tpsprochsave+deltasave;
            end
        end
    end

    ev = 1+(evunif(j)>m1/m0)+(evunif(j)>(m1+m2)/m0);


    if ev==3
        %Si l'�v�nement est une mort par comp�tition

        Indcouple  = ceil(Nvois*Rand1(j,:));
        candcouple = cand(Indcouple,:);
        indmort    = candcouple(ordre1(j));
        exet       = candcouple(ordre2(j));
        candidat   = indpos(indmort);
        pk         = Compet(candidat,indpos(exet));
        respk      = evouinon(j)<pk;
        
        if respk
        
            Cell(indmort,:)=zeros(1,Ngrid);
            bool = cand(1:Nvois,1)~=indmort &  cand(1:Nvois,2)~=indmort;
            temp = cand(bool,:);
            cand = temp;
            temp = Dist(bool);
            Dist = temp;
            indplus         = ind((candidat+1):Size);
            indpos(indplus) = indpos(indplus)-1;
            ind(candidat)   = [];
            inddispo=[inddispo indmort];
            Compet(candidat,:) = [];
            Compet(:,candidat) = [];
            Mres(j,[1 4 5])=[Tps candidat 3];
            Size=Size-1  ;
            Nvois=size(cand,1);
            m1 = m1-gammam ;
            m2 = m2-mu;
            m3 = alpha*Nvois;
            m0 = m1+m2+m3;
        else   Mres(j,[1 5])=[Tps 4];
        end

    elseif ev==2

        candidat=ceil(Size*Rand1(j,:));
        indmort=ind(candidat);
        Cell(indmort,:)=zeros(1,Ngrid);
        bool=cand(1:Nvois,1)~=indmort &  cand(1:Nvois,2)~=indmort;
        temp=cand(bool,:);
        cand=temp;
        temp=Dist(bool);
        Dist=temp;
        indplus=ind((candidat+1):Size);
        indpos(indplus)=indpos(indplus)-1;
        ind(candidat)=[];
        inddispo=[inddispo indmort];
        Compet(candidat,:)=[];
        Compet(:,candidat)=[];
        Mres(j,[1 4 5])=[Tps candidat 2];
        Size=Size-1  ;
        Nvois=size(cand,1);
        m1 = m1-gammam ;
        m2 = m2-mu;
        m3 = alpha*Nvois;
        m0 = m1+m2+m3;
        
    else

        candidat=ceil(Size*Rand1(j,:)); %on choisit le candidat
        taille=Foret(candidat,3);
        tauxN=max(taille-taillep,0)/(taille-taillep)*taille;
        respk=evouinon2(j)<tauxN;
        
        if respk
            %Si l'arbre choisit a atteint sa taille de recrutement

            ArbreNew    = [Foret(ind(candidat),1:2) 0]+Vect(j,:);
            ArbreNew(1) = mod(ArbreNew(1)-Xm,dxM)+Xm;
            ArbreNew(2) = mod(ArbreNew(2)-Ym,dyM)+Ym;

            %Mise � jour de la matrice des �v�nements
            Mres(j,[1:3 5])=[Tps ArbreNew(1:2) 1];

            indcour     = inddispo(1);
            inddispo(1) = [];
            Size        = Size+1 ;
            ind(Size)   = indcour;
            indpos(indcour)  = Size;
            Foret(indcour,:) = ArbreNew';


            % Mise � jour du syst�me de voisinage


            % Rep�rage de l'indice de la cellule du nouvel arbre
        
            nx       = floor((ArbreNew(1)-Xm)/Rx)+1;
            ny       = floor((ArbreNew(2)-Ym)/Ry)+1;
            cellcour = fcouple(nx,ny,Nx,Ny);

            % Affectation de l'arbre � sa cellule            
            Cell(indcour,cellcour)=indcour;

            % Indices des voisins

            % Cellules voisines de la cellule du nouvel arbre
            cellvois = nonzeros(Graph(:,cellcour));
            % voisins potentiels (arbres appartenant aux cellules voisines)
            indvoisp = nonzeros(Cell(ind,cellvois));
            % leur nombre
            Sizev    = size(indvoisp,1);

            % distances du nouvel arbre � ses voisins potentiels
            distemp = distance(ArbreNew,Foret(indvoisp,:),dxM,dyM,Sizev);

            % bool�en (voisins v�ritable ou non, plus pr�cisemment, arbres
            % suceptibles de devenir voisins)
            boolvois=distemp<dMax & distemp~=0;

            % indices des v�ritables voisins
            indvois=indvoisp(boolvois);

            % nombre de v�ritables voisins
            nvoisins=sum(boolvois);
        
        
            if nvoisins~=0
                %Si le nouvel arbre a des voisins

                % distances entre le nouvel arbre et ses voisins v�ritables
                distancesn = distemp(boolvois);
                % Tailles des voisins du nouvel arbre
                r = Foret(indvois,3);
                % Taille du nouvel arbre
                % (dupliqu� autant de fois que de voisins)
                R = Rm*ones(nvoisins,1);
                % Comp�tition entre le nouvel arbre et ses voisins
                temp  = aire(distancesn,r,R);
                temp1 = temp./(pi*r.*r);
                Compet(indpos(indvois),Size)=temp1;
                temp1 = (temp./(pi*R.*R))';
                Compet(Size,indpos(indvois))=temp1;
                % Mise � jour du "graphe de voisinage"
                cand=[cand;[indcour*ones(nvoisins,1) indvois]];
                % Distances entre voisins
                Dist=[Dist ; distancesn];
            end

            Nvois=size(cand,1);

            m1=m1+gammam  ;
            m2=m2+mu;
            m3=alpha*Nvois;
            m0=m1+m2+m3;

        else Mres(j,[1 5])=[Tps 6];
        end

    end

    if Size==0
        disp('Extinction')
        break
    end

    if Size>=NombreMaxmoins
        disp('Population exceeds allocated memory!')
        break
    end


end % fin de la boucle

res=Mres;
save(ress,'res')

temps=toc;

close(h_wair_bar);
end
