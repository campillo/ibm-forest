
function [Temps,Biomasse2,SizeForet]=analyse(indsav)
%--------------------------------------------------------------------------
% fonction permettant d'obtenir, � partir d'un dossier de simulations, 
% les courbes du nombre d'arbre en fonction du temps et de la somme des 
% aires couvertes par les arbres sur l'aire totale de la for�t.
%
% Entr�e:
%   indsav    : clef de la simulation (cha�ne de caract�res)
% Sorties:
%	Temps     : le temps pour chaque fichier de simulation   
%	Biomasse2 : le vecteur de la somme des surfaces couvertes 
%               par les arbres sur la surface totale pour chaque 
%               fichier de simulation
%	SizeForet : le vecteur du nombre d'arbres pour chaque fichier 
%               de simulation	
%--------------------------------------------------------------------------


chemin=(['simulforest_' indsav]);
cheminsave=[chemin '/save/save'];

load([chemin '/paramsim.mat']);
numinstr=size(param,1);

numfich   = param(numinstr,21);
Temps     = zeros(numfich+1,1);
Biomasse2 = zeros(numfich+1,1);
SizeForet = zeros(numfich+1,1);

s=0;

for i=1:numinstr

    deltasave    = param(i,15);
    numfichdebut = param(i,20);
    numfichfin   = param(i,21);
    Tps          = param(i,18);

    for k=numfichdebut:numfichfin

        s=s+1;
        load([cheminsave num2str(k) '.mat'])
        X=Xs;
        Size    = size(X);
        tailles = X(:,3);

        if  k~=param(1,20)
            Tps=Tps+deltasave;
        end

        Biomasse2(s) = pi*sum(tailles.^2);
        SizeForet(s) = Size(1);
        Temps(s)     = Tps;

    end
end


end


 
 
