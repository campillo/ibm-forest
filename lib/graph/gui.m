function fh=gui(clef)
%--------------------------------------------------------------------------
% fonction lan�ant l'interface de sortie (voir document d�crivant 
% l'interface)
%
% Entr�e:
%	clef : clef de la simulation (cha�ne de caract�res)
%--------------------------------------------------------------------------

param=[];
Xs=[];
a='filesave';
chemin=(['simulforest_' clef]);
cheminsave=[chemin '/save/save'];
load([chemin '/paramsim.mat']);
load([cheminsave num2str(0) '.mat'])

numinstr=size(param,1);
fin=param(numinstr,19);

slider.val=1;

[Temps,Biomasse2,SizeForet]=analyse(clef);
Taille=SizeForet;

N=length(Temps);

stop=false;


fh=figure('color',[0.95 0.95 0.95],'name','Menu1','units','normalized','Position',[0.2   0.2   0.9   0.9]);
sh=uicontrol(fh,'Style','slider','units','normalized','position',[0.06 0.550 0.911 0.01],...
    'callback',@slider_callback2,...,
    'Max',N,'Min',1,'SliderStep',[0.0 0.02],'Value',round(slider.val));
uicontrol('style','pushbutton','units','normalized','position',[0.6 0.510 0.05 0.02],'string','Play','callback',@plotres)
uicontrol('style','pushbutton','units','normalized','position',[0.65 0.510 0.05 0.02],'string','Stop','callback',@stop2)
uicontrol('style','pushbutton','units','normalized','position',[0.80 0.510 0.05 0.02],'string','Film','callback',@film)
uicontrol('style','pushbutton','units','normalized','position',[0.70 0.510 0.05 0.02],'string','End','callback',@finsim)
uicontrol('style','pushbutton','units','normalized','position',[0.75 0.510 0.05 0.02],'string','Capture','callback',@capt)
uicontrol('style','pushbutton','units','normalized','position',[0.95 0.0 0.05 0.02],'string','Quit','callback',@quit)


h1=axes('units','normalized','position',[0.064 0.780 0.903 0.180]);
h2=axes('units','normalized','position',[0.064 0.580 0.903 0.180]);
h3=axes('units','normalized','position',[0.064 0.079 0.450 0.450]);
h4=axes('units','normalized','position',[0.7 0.2 0.1 0.3],'Visible','off');

plotXM(Temps,Taille,Biomasse2,h1,h2,h3,h4,slider.val,clef,fin);

guidata(fh,slider);

end


%--------------------------------------------------------------------------
function slider_callback2(hObject,eventdata)
slider = guidata(fh);  % Get GUI data.
slider.val = round(get(hObject,'Value'));
plotXM(Temps,Taille,Biomasse2,h1,h2,h3,h4,slider.val,clef,fin) ;
guidata(fh,slider) % Save GUI data before returning.
end

%--------------------------------------------------------------------------
function film(hObject,eventdata)
plotfilm(clef)
end

%--------------------------------------------------------------------------
function capt(hObject,eventdata)
slider = guidata(fh);
hgsave(['simulforest_' clef '/fig/' num2str(slider.val)])
end

%--------------------------------------------------------------------------
function finsim(hObject,eventdata)
stop=true;
set(sh,'Value',N);
slider.val=N;
plotXM(Temps,Taille,Biomasse2,h1,h2,h3,h4,N,clef,fin);
guidata(fh,slider) % Save GUI data before returning.
end

%--------------------------------------------------------------------------
%function plotres(X0,Mres,pos,h1,h2,h3,sh)
function plotres(hObject,eventdata)
stop=false;

% slider.val = get(hObject,'Value');
%--------------------------------------------------------------------------
% fonction de trace blabla
%   Mres   : xxx
%   X0,T0  : xxx
%   Xm,XM  : xxx
%   Ym,YM  : xxx
%--------------------------------------------------------------------------
% cr�� par N.Desassis
% date
% modifi� le xxx par xxxx
%--------------------------------------------------------------------------

if slider.val==N  
    % Si on �tait � la fin, on repart du d�but
    set(sh,'Value',1);
    slider.val=1;
end

for k=slider.val:N
    slider.val=k;
    if ~stop
        set(sh,'Value',k);
        plotXM(Temps,Taille,Biomasse2,h1,h2,h3,h4,k,clef,fin);
    else
        break;
    end
    slider=guidata(fh);
    if slider.val~=k
        % Si l'utilisateur a boug� le slide entre temps
        k=slider.val;
    end
end

end


%--------------------------------------------------------------------------
function quit(hObject,eventdata)
close(fh);
end


%--------------------------------------------------------------------------
function stop2(hObject,eventdata)
slider = guidata(fh);
stop=true;
guidata(fh,slider)
end









    

