function plotXM(Temps,Taille,Biomasse2,h1,h2,h3,h4,k,indsav,fin)
%--------------------------------------------------------------------------
% fonction permettant de tracer les figures de l'interface de sortie � un 
% instant donn�
%
% Entr�es:
%	Temps     : un vecteur (sortie de la fonction analyse)
%	Taille    : un vecteur (sortie de la fonction analyse)
%	Biomasse2 : un vecteur (sortie de la fonction analyse)
%	h1        : un objet de type figure matlab
%	h2	      : un objet de type figure matlab
%	h3	      : un objet de type figure matlab
%	h4	      : un objet de type figure matlab
%	k	      : un entier (indice du fichier de sauvegarde pour lequel 
%               on veut tracer la for�t)
%	indsav    : clef de la simulation (cha�ne de caract�res)
%	fin	      : indice du dernier fichier de sauvegardes
%--------------------------------------------------------------------------
    
chemin     = (['simulforest_' indsav]);
cheminsave = [chemin '/save/save'];
load([chemin '/paramsim.mat']);

numinstr=size(param,1);

temps=Temps(k);

for i=1:numinstr
    if temps>=param(i,18) && temps<=param(i,19)
        ligne=i;
    end
end

Xm = param(ligne,9);
XM = param(ligne,10);
Ym = param(ligne,11);
YM = param(ligne,12);
    
Biomasse2    = Biomasse2/((YM-Ym)*(XM-Xm));
deltasave    = param(ligne,15);
numfichdebut = param(ligne,20);
Tps          = param(ligne,18)+(k-1-numfichdebut+(ligne>1))*deltasave;

L  = linspace(0,2.*pi,100); 
xv = cos(L)';
yv = sin(L)';
xv = [xv ; xv(1)]; 
yv = [yv ; yv(1)];

axes(h1)
cla;
plot(Temps,Taille);
hold on;
axis([0 max(Temps) 0 max(Taille)]);
line(Temps(1:k),Taille(1:k),'Color','r')
hold on;
line([Temps(k) Temps(k)],[0 max(Taille)],'Color','r')
hold on;
axes(h2)
cla;
plot(Temps,Biomasse2);
hold on;
axis([0 max(Temps) 0 max(Biomasse2)]);
line(Temps(1:k),Biomasse2(1:k),'Color','r')
hold on;
line([Temps(k) Temps(k)],[0 max(Biomasse2)],'Color','r')
hold on;

if numinstr>1
    % Si plusieurs scenarios, lignes horizontales entre les scenarios
    for i=2:numinstr
        if i<=ligne
            col='r';
        else
            col='b';
        end
        axes(h1);
        line(param([i i],18),get(h1,'Ylim'),'Color',col);
        hold on;
        axes(h2)
        line(param([i i],18),get(h2,'Ylim'),'Color',col);
        hold on;
    end
end

axes(h3)
cla;
load([cheminsave num2str(k-1) '.mat'])
X       = Xs;
Size    = size(X);
tailles = X(:,3);

for j=1:Size(1)
    line(X(j,1)+tailles(j)*xv,X(j,2)+tailles(j)*yv);
    hold on;
end

axis equal;
% axis square ;
axis([Xm,XM,Ym,YM]);
drawnow;
Fs=14;
axes(h4);
cla;

text(0.1,0.5,['Temps �coul� : '  num2str(round(Tps*10)/10) ' ans sur ' num2str(round(fin*10)/10) ' ans'],'FontSize',Fs);
text(0.1,0.2,['Taille pop: ' num2str(Size(1)) ' arbres'],'FontSize',Fs);
text(0.1,0.8,['Scenario ' num2str(ligne)],'FontSize',Fs);

end
