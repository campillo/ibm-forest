function plotfilm(indsav)
%--------------------------------------------------------------------------
% fonction permettant de r�aliser le film pour une simulation effectu�e 
% (le film sera cr�e dans le r�pertoire de la simulation)
%
% Entr�e:
%		clef : clef de la simulation (cha�ne de caract�res)
%--------------------------------------------------------------------------

chemin=(['simulforest_' indsav]);
cheminsave=[chemin '/save/save'];

load([chemin '/paramsim.mat']);
numinstr=size(param,1);
fin=param(numinstr,19);
h_wair_bar = waitbar(0,'Please wait...'); % wait bar
numfich=param(numinstr,21);

L = linspace(0,2.*pi,100); xv = cos(L)';yv = sin(L)';
xv = [xv ; xv(1)]; yv = [yv ; yv(1)];

aviobj=avifile([chemin '/movie_' indsav '.avi'],'fps',8,'quality',100); 
hf= figure('visible','off','DoubleBuffer','on','units','normalized','Position',[1   1   1   1],'Renderer','zbuffer');
hax=axes;

s=0;

for i=1:numinstr

    deltasave    = param(i,15);
    numfichdebut = param(i,20);
    numfichfin   = param(i,21);
    Tps  = param(i,18);
    Xm   = param(i,9);
    XM   = param(i,10);
    Ym   = param(i,11);
    YM   = param(i,12);
    postextx    = 0.1*(XM-Xm);
    postexty1   = 0.95*YM;
    postexty2   = 0.90*YM;
    postexty3   = 0.85*YM;
    
    for k=numfichdebut:numfichfin

        if i~=1 || k~=numfichdebut
            Tps=Tps+deltasave;
        end
        
        load([cheminsave num2str(k) '.mat'])

        waitbar(s/numfich)

        load([cheminsave num2str(s) '.mat'])
        s=s+1;
        waitbar(s/numfich)
        X=Xs;
        Size=size(X);
        tailles=X(:,3);

        for j=1:Size(1)
            line(X(j,1)+tailles(j)*xv,X(j,2)+tailles(j)*yv,'Parent',hax);
            hold on;
        end
        
        axis square ;
        axis([Xm,XM,Ym,YM]);
        text(postextx,postexty1,['Scenario ' num2str(i)],'Parent',hax);
        text(postextx,postexty2,['Temps �coul� : '  num2str(round(Tps*10)/10) ' ans sur ' num2str(round(fin*10)/10) ' ans'],'Parent',hax);
        text(postextx,postexty3,['Taille pop: ' num2str(Size(1)) ' arbres'],'Parent',hax);
        aviobj=addframe(aviobj,hf);
        cla;
    end

end

aviobj=close(aviobj);
close(hf);
close(h_wair_bar);
