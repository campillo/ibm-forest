function forest
%--------------------------------------------------------------------------
% La fonction forest affiche le menu principal.
% Il y a deux types de menu principaux selon que des dossiers dont le nom
% commence par simulforest_ existent ou non dans le dossier de travail.
%--------------------------------------------------------------------------

chemin='forest/';
addpath(chemin);
addpath([chemin '/lib/graph']);
addpath([chemin '/lib/build']);
sortie=false;

while ~sortie
    a=dir('simulforest*');
    c=struct2cell(a);

    if ~isempty(c)
        affichexist();

        fprintf(['Dans n''importe quel menu, vous pouvez taper ''help'' pour avoir des pr�cisions sur ce menu.\n\n'...
            '> Que voulez vous faire? \n'...
            '  1. Effectuer une nouvelle simulation\n'...
            '  2. Travailler sur une simulation existante \n'...
            '  q. Quitter \n']);
        reponse=input('  Choix [q] : ','s');


        switch reponse
            case '1'
                creersim;
            case '2'
                menuchoixdelasim;
            case 'help'
                aide_menuinitial1
            otherwise
                sortie=true;
        end

    else

        fprintf(['-------------------------------------------------------------------------------    \n' ...
            'Actuellement, le dossier ne contient aucun fichier de simulations. \n' ...
            '\n'])
        fprintf(['Dans n''importe quel menu, vous pouvez taper ''help'' pour avoir des pr�cisions sur ce menu.\n\n'...
            '> Que voulez vous faire? \n'...
            '  1. Effectuer une nouvelle simulation\n'...
            '  q. Quitter \n']);
        reponse=input('  Choix [q] : ','s');

        if isempty(reponse); reponse='q';end
        switch reponse
            case '1'
                creersim;
            case 'help'
                aide_menuinitial2
            otherwise
                sortie=true;
        end

    end
end
end


%--------------------------------------------------------------------------
function menuchoixdelasim
sortie=false;
while ~sortie
    fprintf(['    \n' ...
        '     > Donner la clef de la simulation sur laquelle vous voulez travailler ou quitter (''q'') : ']);
    numsim=input('','s');
    if ~isempty(numsim)
        a=dir('simul*');
        c=struct2cell(a);
        if all(numsim~='q') && ~strcmpi(numsim,'help')
            if ~ismember(['simulforest_' numsim],c(1,:))
                fprintf(['       Le fichier simulforest_' numsim ' n''existe pas.']);
            else
                sortie=true;
                continu(numsim,false);
                analysesim(numsim);
            end
        elseif all(numsim~='q')
            aide_menuchoixdelasim
        else
            sortie=true;
        end
    end
end
end


%--------------------------------------------------------------------------
function [sortie]=confirm(numsim)
sortie=false;
while ~sortie
    fprintf(['\n'...
        '          > Un fichier de simulation simulforest_' numsim ' existe d�j�. Voulez-vous l''�craser? 1.Oui,2.Non [2] : ']);
    reponse=input('','s');
    switch reponse
        case '1'
            sortie=true;
            continu(numsim,true);
        case '2'
            sortie=true;
        case 'help'
            aide_confirm
    end
end
if all(reponse=='2')
    sortie=false;
end
end


%--------------------------------------------------------------------------
function creersim
sortie=false;
while ~sortie
    fprintf(['   \n'...
        '    > Donner une clef pour cr�er le fichier de simulation \n'...
        '     (le fichier portera le nom simulforest_clef) ou quitter (''q'') :\n']);
    numsim=input('     Clef [q] : ','s');
    if isempty(numsim)
        numsim='q';
    end
    if all(numsim~='q') && ~strcmpi(numsim,'help');
        a=dir('simulforest_*');
        c=struct2cell(a);
        if ismember(['simulforest_' numsim],c(1,:))
            [sortie]=confirm(numsim);
        else
            sortie=true;
            continu(numsim,true);
        end
    elseif all(numsim~='q')
        aide_creersim
    else sortie=true;
    end
end
end


%--------------------------------------------------------------------------
function askmodif(numsim,new,afficheparam)
premiere=true;
sortie=false;
chaine=makestring(numsim,new);  
while  ~sortie
	if afficheparam 
        fprintf(chaine); 
    end
afficheparam=true;
fprintf(['             Que voulez-vous faire?'...
         '\n'...
         '              1. Modifier un param�tre \n'...
         '              2. Lancer la simulation (avec les param�tres actuels) \n'...
         '              q. Quitter (annule toutes les modifications de param�tres)\n'...
          ]);
 
reponse=input('              Choix [1] : ','s');

switch reponse
    case '2'  
        ok=verifparam(numsim);
        ok1=ok;
        while ~ok1 && ~sortie
            sortie=modifparamcorrect(numsim,new);
            if ~sortie
                ok1 =verifparam(numsim);
            end
        end
        if ok
            sortie=true;
            chaine=makestring(numsim,new);  
            h=fopen(['simulforest_' numsim '/parametres.txt'],'w','n','latin1');
            fwrite(h,chaine,'char');
            fclose(h);
            simultot(new,numsim);
            finsim(numsim);
        elseif ok1
            fprintf(['\n------------------------------------------------------------------\n\n'...
                 '            Les param�tres sont maintenant conformes.\n\n']) 
         
        end

    case 'q'
        sortie=true;
        if new
            rmdir(['simulforest_' numsim],'s');
        else 
            load(['simulforest_' numsim '/paramsim.mat']);   
            param(size(param,1),:)=[];
            save(['simulforest_' numsim '/paramsim.mat']);      
        end

    case 'help'
        aide_askmodif(numsim,new)
    
    otherwise
        [sortie]=modifparam(numsim,new,premiere);
        premiere=false;
        fprintf(['    \n' ...
        '          > Les param�tres enregistr�s pour simulforest_' numsim ' sont les suivants : \n'...
        '\n']);
        chaine=makestring(numsim,new);  
    end        
end
end


%--------------------------------------------------------------------------
function sortie=modifparamcorrect(numsim,new)
sortie  = false;
sortie2 = false;
while ~sortie2
fprintf(['             Que voulez-vous faire?'...
         '\n'...
         '              1. Modifier un param�tre \n'...
         '              2. Afficher les param�tres \n'...
         '              q. Quitter (annule toutes les modifications de param�tres)\n'...
          ]);
 
 reponse=input('              Choix [2] : ','s');
 if all(reponse~='q')
     switch reponse

         case '1'
             modifparam(numsim,new,false);
             % fprintf(['    \n' ...
             % '          > Les param�tres enregistr�s pour simulforest_' numsim ' sont les suivants : \n'...
             % '\n']);
             % chaine=makestring(numsim,new);
             % fprintf(chaine);
             % sortie=true;
             sortie2=true;

         case 'help'
             aide_modifparamcorrect(numsim,new)

         otherwise
             fprintf('\n\n')
             chaine=makestring(numsim,new);
             fprintf(chaine);
     end

 else

     sortie  = true;
     sortie2 = true;

     if new
         rmdir(['simulforest_' numsim],'s');
     else
         load(['simulforest_' numsim '/paramsim.mat']);
         param(size(param,1),:)=[];
         save(['simulforest_' numsim '/paramsim.mat']);
     end
     
 end
end
end


%--------------------------------------------------------------------------
function finsim(numsim)
sortie=false;
while ~sortie
    fprintf(['\n'...
        '               > Que voulez-vous faire? \n'...
        '                 1. Continuer � travailler sur la simulation simulforest_' numsim '\n'...
        '                 2. Retourner au menu principal\n']);
    reponse =input('                 Choix [1] : ','s');

    if strcmpi(reponse,'help')
        aide_finsim(numsim)
    elseif      ~strcmpi(reponse,'2')
        sortie=true;
        analysesim(numsim);
    else
        sortie=true;
    end
end
end


%--------------------------------------------------------------------------
function continu(numsim,new)
if new
    initforest(numsim);
    fprintf(['    \n' ...
        '          > Les param�tres par d�faut sont les suivants : \n'...
        '\n']);
    askmodif(numsim,new,true);
else
    load(['simulforest_' numsim '/paramsim.mat']);
    numinstr=size(param,1);
    if numinstr>1
        chaine=' scenarios diff�rents';
    else
        chaine=' scenario';
    end

    fprintf([' \n' ...
        '             > La simulation simulforest_' numsim ' a �t� calcul�e selon ' num2str(numinstr) chaine '\n'...
        '               dont les param�tres sont les suivants : \n'...
        '\n']);
    chaine=makestring(numsim,new);
    fprintf(chaine);
end
end


%--------------------------------------------------------------------------
function analysesim(numsim)
sortie=false;
while ~sortie
    fprintf(['\n'...
        '               > Que voulez-vous faire?\n'...
        '                 1. Interface graphique de sortie de simulforest_' numsim '\n'...
        '                 2. Continuer simulforest_' numsim '\n'...
        '                 3. Afficher les param�tres utilis�s pour simulforest_' numsim '\n'...
        '                 q. Quitter'...
        '\n']);

    reponse=input('                 Choix [3] : ','s');

    a=who;
    if ismember('fh',a)
        close(fh);
    end

    switch reponse
        case 'help'
            aide_analysesim(numsim)
        case '1'
            gui(numsim);
        case '2'
            continu(numsim,false);
            askmodif(numsim,false,false);
        case 'q'
            sortie=true;
        otherwise
            chaine=makestring(numsim,false);
            fprintf(chaine)
    end
end
end


%--------------------------------------------------------------------------
function chaine=makestring(numsim,new)
load(['simulforest_' numsim '/paramsim.mat']);
numinstr=size(param,1);
taillep=length(num2str(numinstr));
for i=[1:17 19 21]
    for j=1:numinstr
        temp=num2str(param(j,i));
        taillep=max(taillep,length(temp)+1);
    end
end
paramname=zeros(20,taillep*numinstr);
k=0;
for i=[1:17 19 21]
    k=k+1;
    var=[];
    for j=1:numinstr

        temp=num2str(param(j,i));
        while length(temp)<taillep
            temp=[' ' temp];
        end
        var=[var temp];

    end
    paramname(k,:)=var;
end
var=[];
for i=1:numinstr
    temp=num2str(i);
    while length(temp)<taillep
        temp=[' ' temp];
    end
    var=[var temp];
end
paramname(20,:)=var;

if ~new
    chainecontinue=['                                                         Scenario   '  paramname(20,:)  '\n'...
        '                                                  Dernier fichier   '  paramname(19,:)  '\n'...
        '                                                     Temps de fin   '  paramname(18,:)  '\n'...
        '\n'];
else
    chainecontinue='';
end

chaine=[chainecontinue ...
    '                Param�tres du mod�le : \n'...
    '\n'...
    '              1.Rayon minimum d''un arbre                          : '  paramname(1,:) '\n'...
    '              2.Rayon maximum d''un arbre                          : '  paramname(2,:) '\n'...
    '              3.Taux de croissance                                : '   paramname(3,:) '\n'...
    '              4.Rayon de recrutement                              : '   paramname(4,:) '\n'...
    '              5.Taux de production de graine                      : '   paramname(5,:) '\n'...
    '              6.Param�tre de dispersion                           : '   paramname(6,:) '\n'...
    '              7.Taux de mort naturelle                            : '   paramname(7,:) '\n'...
    '              8.Taux de mort par comp�tition                      : '   paramname(8,:) '\n'...
    '\n'...
    '                Autres param�tres  : \n'...
    '\n'...
    '              9.Abscisse du coin inf�rieur gauche                 : '   paramname(9,:)  '\n'...
    '             10.Abscisse du coin sup�rieur droit                  : '   paramname(10,:) '\n'...
    '             11.Ordonn�e du coin inf�rieur gauche                 : '   paramname(11,:) '\n'...
    '             12.Ordonn�e du coin sup�rieur droite                 : '   paramname(12,:) '\n'...
    '             13.Nombre initial d''individus                        : '  paramname(13,:) '\n'...
    '             14.Temps entre deux mises � jour des tailles         : '   paramname(14,:) '\n'...
    '             15.Temps entre deux sauvegardes                      : '   paramname(15,:) '\n'...
    '             16.Nombre d''it�rations de l''algorithme               : ' paramname(16,:) '\n'...
    '             17.Nombre maximum d''individus (m�moire disponible)   : '  paramname(17,:) '\n'...
    '\n'...
    ];

end


%--------------------------------------------------------------------------
function [sortie]=modifparam(numsim,new,premiere)
sortie=false;
load(['simulforest_' numsim '/paramsim.mat']);
numinstr=size(param,1);

if ~new && premiere
    param    = [param;param(numinstr,:)];
    numinstr = numinstr+1;
    param(numinstr,18) = param(numinstr-1,19);
    param(numinstr,20) = param(numinstr-1,21)+1;
end

%chaine=makestring(numsim,new);
%fprintf(chaine);
sortie2    = false;
secondmenu = false;

while ~sortie2
    fprintf(['\n'...
        '                   > Indiquez le num�ro du param�tre que vous souhaitez modifier (''q'' pour quitter) :\n']);
    numpar=input('                     Choix : ','s');
    if isempty(numpar)
        fprintf('                     Vous n''avez saisi aucune valeur.');

    else
        switch numpar
            case 'help'
                fprintf(['\n\n                    Saisissez un entier entre 1 et 17 correspondant � l''indice du param�tre que vous souhaitez modifier\n'...
                    '                    ou quittez (''q'').\n'])
                fprintf('\n                  ENTREE pour continuer\n\n');
                input('','s');
            case{'1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17'}
                secondmenu=true;
                switch numpar

                    case '1'
                        nomparam='le rayon minimum d''un arbre vaut ' ;
                        validparam1='                         Le rayon minimum doit �tre un r�el positif.\n';
                        validparam2='                         Entrez une valeur valide (''q'' pour quitter)';
                    case '2'
                        nomparam='le rayon maximum d''un arbre vaut ' ;
                        validparam1='                         Le rayon maximum doit �tre un r�el positif.\n';
                        validparam2='                         Entrez une valeur valide (''q'' pour quitter)';
                    case '3'
                        nomparam='le taux de croissance vaut ';
                        validparam1='                         Le taux de croissance doit �tre un r�el positif.\n';
                        validparam2='                         Entrez une valeur valide (''q'' pour quitter)';
                    case '4'
                        nomparam='le rayon de recrutement vaut '  ;
                        validparam1='                         Le rayon de recrutement doit �tre un r�el positif.\n';
                        validparam2='                         Entrez une valeur valide (''q'' pour quitter)';
                    case '5'
                        nomparam='le taux de production de graine vaut ';
                        validparam1='                         Le taux de production de graines doit �tre un r�el positif.\n';
                        validparam2='                         Entrez une valeur valide (''q'' pour quitter)';
                    case '6'
                        nomparam='le param�tre de dispersion vaut '  ;
                        validparam1='                         Le param�tre de dispersion doit �tre un r�el positif.\n';
                        validparam2='                         Entrez une valeur valide (''q'' pour quitter)';
                    case '7'
                        nomparam='le taux de mort naturelle vaut ' ;
                        validparam1='                         Le taux de mort naturelle doit �tre un r�el positif.\n';
                        validparam2='                         Entrez une valeur valide (''q'' pour quitter)';
                    case '8'
                        nomparam='le taux de mort par comp�tition vaut ' ;
                        validparam1='                         Le taux de mort par comp�tition doit �tre un r�el positif.\n';
                        validparam2='                         Entrez une valeur valide (''q'' pour quitter)';
                    case '9'
                        nomparam='l''abscisse du coin inf�rieur gauche vaut '  ;
                        validparam1='                         L''abscisse du coin inf�rieur gauche doit �tre un nombre r�el.\n';
                        validparam2='                         Entrez une valeur valide (''q'' pour quitter)';
                    case '10'

                        nomparam='l''abscisse du coin sup�rieur droit vaut ';
                        validparam1='                         L''abscisse du coin sup�rieur droit doit �tre un nombre r�el.\n';
                        validparam2='                         Entrez une valeur valide (''q'' pour quitter)';
                    case '11'
                        nomparam='l''ordonn�e du coin inf�rieur gauche vaut '  ;
                        validparam1='                         L''ordonn�e du coin inf�rieur gauche doit �tre un nombre r�el.\n';
                        validparam2='                         Entrez une valeur valide (''q'' pour quitter)';
                    case '12'
                        nomparam='l''ordonn�e du coin sup�rieur droit vaut '  ;
                        validparam1='                         L''ordonn�e du coin sup�rieur droit doit �tre un nombre r�el.\n';
                        validparam2='                         Entrez une valeur valide (''q'' pour quitter)';
                    case '13'
                        nomparam='le nombre initial d''individus est fix� � ' ;
                        validparam1='                         Le nombre initial d''individus doit �tre un entier positif.\n';
                        validparam2='                         Entrez une valeur valide (''q'' pour quitter)';
                    case '14'
                        nomparam='le temps entre deux mises � jour des tailles est fix� � ';
                        validparam1='                         Le temps entre deux mises � jour des tailles doit �tre un r�el positif.\n';
                        validparam2='                         Entrez une valeur valide (''q'' pour quitter)';
                    case '15'
                        nomparam='le temps entre deux sauvegardes est fix� � '  ;
                        validparam1='                         Le temps entre deux sauvegardes doit �tre un r�el positif.\n';
                        validparam2='                         Entrez une valeur valide (''q'' pour quitter)';
                    case '16'
                        nomparam='le nombre d''it�rations de l''algorithme est fix� � '  ;
                        validparam1='                         Le nombre d''it�rations de l''algorithme doit �tre un entier positif.\n';
                        validparam2='                         Entrez une valeur valide (''q'' pour quitter)';
                    case '17'
                        nomparam='le nombre maximum d''individus est fix� � '  ;
                        validparam1='                         Le nombre maximum d''individus doit �tre un entier positif.\n';
                        validparam2='                         Entrez une valeur valide (''q'' pour quitter)';
                end
            case 'q'
                sortie2=true;
                %   sortie=true;
            otherwise
                fprintf('                     La valeur doit �tre un entier entre 1 et 17.');
        end
    end


    if secondmenu
        while ~sortie2
            fprintf(['\n'...
                '                        > Actuellement, ' nomparam num2str(param(numinstr,str2double(numpar))) '.\n'...
                '                         Quelle valeur voulez-vous lui donner (''q'' pour quitter)? \n'])
            valpar=input('                         Nouvelle valeur : ','s');

            if isempty(valpar)
                fprintf('                         Vous n''avez saisi aucune valeur.')
            elseif all(valpar=='q')
                sortie2=true;
            elseif strcmpi(valpar,'help')
                fprintf('\n\n                         Entrez la nouvelle valeur pour le param�tre.\n')
                fprintf(validparam1)
                fprintf('\n ENTREE pour continuer\n\n');
                input('','s');
            else
                switch numpar
                    case '1'
                        while all(valpar~='q') && (isempty(str2num(valpar)) || str2num(valpar)<=0)
                            fprintf(validparam1)
                            fprintf(validparam2)
                            valpar=input(' : ','s');
                        end
                    case '2'
                        while all(valpar~='q') && (isempty(str2num(valpar)) || str2num(valpar)<=0)
                            fprintf(validparam1)
                            fprintf(validparam2)
                            valpar=input(' : ','s');
                        end
                    case '3'
                        while all(valpar~='q') && (isempty(str2num(valpar)) || str2num(valpar)<=0)
                            fprintf(validparam1)
                            fprintf(validparam2)
                            valpar=input(' : ','s');
                        end
                    case '4'
                        while all(valpar~='q') && (isempty(str2num(valpar)) || str2num(valpar)<=0)
                            fprintf(validparam1)
                            fprintf(validparam2)
                            valpar=input(' : ','s');
                        end
                    case '5'
                        while all(valpar~='q') && (isempty(str2num(valpar)) || str2num(valpar)<=0)
                            fprintf(validparam1)
                            fprintf(validparam2)
                            valpar=input(' : ','s');
                        end
                    case '6'
                        while all(valpar~='q') && (isempty(str2num(valpar)) || str2num(valpar)<=0)
                            fprintf(validparam1)
                            fprintf(validparam2)
                            valpar=input(' : ','s');
                        end
                    case '7'
                        while all(valpar~='q') && (isempty(str2num(valpar)) || str2num(valpar)<=0)
                            fprintf(validparam1)
                            fprintf(validparam2)
                            valpar=input(' : ','s');
                        end
                    case '8'
                        while all(valpar~='q') && (isempty(str2num(valpar)) || str2num(valpar)<=0)
                            fprintf(validparam1)
                            fprintf(validparam2)
                            valpar=input(' : ','s');
                        end
                    case '9'
                        while all(valpar~='q') && isempty(str2num(valpar))
                            fprintf(validparam1)
                            fprintf(validparam2)
                            valpar=input(' : ','s');
                        end
                    case '10'
                        while all(valpar~='q') && isempty(str2num(valpar))
                            fprintf(validparam1)
                            fprintf(validparam2)
                            valpar=input(' : ','s');
                        end
                    case '11'
                        while all(valpar~='q') && isempty(str2num(valpar))
                            fprintf(validparam1)
                            fprintf(validparam2)
                            valpar=input(' : ','s');
                        end
                    case '12'
                        while all(valpar~='q') && isempty(str2num(valpar))
                            fprintf(validparam1)
                            fprintf(validparam2)
                            valpar=input(' : ','s');
                        end
                    case '13'
                        while all(valpar~='q') && (isempty(str2num(valpar)) || str2num(valpar)<=0 ||round(str2num(valpar))~=str2num(valpar))
                            fprintf(validparam1)
                            fprintf(validparam2)
                            valpar=input(' : ','s');
                        end
                    case '14'
                        while all(valpar~='q') && (isempty(str2num(valpar)) || str2num(valpar)<=0)
                            fprintf(validparam1)
                            fprintf(validparam2)
                            valpar=input(' : ','s');
                        end
                    case '15'
                        while all(valpar~='q') && (isempty(str2num(valpar)) || str2num(valpar)<=0)
                            fprintf(validparam1)
                            fprintf(validparam2)
                            valpar=input(' : ','s');
                        end
                    case '16'
                        while all(valpar~='q') && (isempty(str2num(valpar)) || str2num(valpar)<=0 ||round(str2num(valpar))~=str2num(valpar))
                            fprintf(validparam1)
                            fprintf(validparam2)
                            valpar=input(' : ','s');
                        end
                    case '17'
                        while all(valpar~='q') && (isempty(str2num(valpar)) || str2num(valpar)<=0 ||round(str2num(valpar))~=str2num(valpar))
                            fprintf(validparam1)
                            fprintf(validparam2)
                            valpar=input(' : ','s');
                        end
                end

                sortie2=true;
                
                if all(valpar~='q')
                    param(numinstr,str2double(numpar))=str2num(valpar);
                    save(['simulforest_' numsim '/paramsim.mat'],'param');
                end
                
            end

        end
    end
end
end
        
      
%--------------------------------------------------------------------------
function ok=verifparam(numsim)

ok=true;
load(['simulforest_' numsim '/paramsim.mat']);   
numinstr=size(param,1);

rm = param(numinstr,1);
rM = param(numinstr,2);
rc = param(numinstr,4);
Xm = param(numinstr,9);
XM = param(numinstr,10);
Ym = param(numinstr,11);
YM = param(numinstr,12);
Size = param(numinstr,13);
nombreMax = param(numinstr,17);

if rm>rM
    ok=false;
    fprintf(['\n'...'
        '              Le rayon minimum d''un arbre doit �tre inf�rieur au rayon maximum.'])
end
  
if rc>rM ||rc<rm
    ok=false;
     fprintf(['\n'...'
     '              Le rayon de recrutement doit �tre compris entre le rayon minimum et le rayon maximum.'])
end  

 if Xm>=XM
    ok=false;
    fprintf(['\n'...'
    '              L''abscisse du coin inf�rieur gauche doit �tre un nombre r�el inf�rieur � l''abscisse du coin sup�rieur droit.']) 
 end
 
 if Ym>=YM
     ok=false;
     fprintf(['\n'...'
         '              L''ordonn�e du coin inf�rieur gauche doit �tre un nombre r�el inf�rieur � l''ordonn�e du coin sup�rieur droit.'])
 end
 if Size>nombreMax
     ok=false;
     fprintf(['\n'...'
         '              Le nombre maximum d''individus doit �tre sup�rieur au nombre initial d''individus.'])
 end

 if ~ok
     fprintf(['\n'...'
         '              La simulation ne peut pas �tre lanc�e. Modifiez les param�tres ou quittez.\n\n\n'])
 end

end



%--------------------------------------------------------------------------
function affichexist()
a=dir('simulforest_*');
c=struct2cell(a);
if ~isempty(c(1,:))
    elt1='';
    for i=1:length(c(1,:))
        elt1=[elt1 c{1,i} ' '];
    end
end


fprintf(['---------------------------------------------------------------------------    \n' ...
  'Actuellement, le dossier contient le(s) fichier(s) suivant(s) :\n' ...
  '\n'...
   elt1 '\n\n']);
end


%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
%fonctions d'aide
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
function aide_menuinitial1
fprintf(['\n'...  
         '        1.Effectuer une nouvelle simulation : \n'...
         '             Ce menu permet de cr�er une nouvelle simulation,\n'...
         '             de changer les param�tres par d�faut (ceux enregistr�s dans ../forest/initforest.m)\n'...
         '             et de lancer la simulation.\n'...
         '        2. Travailler sur une simulation existante :\n'...
         '             Ce menu permet d''analyser les r�sultats d''une simulation d�j� effectu�e,\n'...
         '             de faire le film de cette simulation, \n'...
         '             ou de la continuer en changeant �ventuellement les param�tres.\n'...
         '\n'...
         '\n'...
         'ENTREE pour continuer\n\n']);
     input('','s'); 
end


%--------------------------------------------------------------------------
function aide_menuinitial2
fprintf(['\n'...  
         '        1.Effectuer une nouvelle simulation : \n'...
         '             Ce menu permet de cr�er une nouvelle simulation,\n'...
         '             de changer les param�tres par d�faut (ceux enregistr�s dans ../forest/initforest.m),\n'...
         '             et de lancer la simulation.\n'...
         '\n'...
         '\n'...
         'ENTREE pour continuer\n\n']);
     input('','s');     
end


%--------------------------------------------------------------------------
function aide_menuchoixdelasim
fprintf(['\n'...  
         '        Les dossiers de simulation portent un nom de la forme simulforest_clef.\n'...
         '        Tapez ''clef'' pour travailler sur la simulation simulforest_clef.\n'...
         '        ou ''q'' pour revenir au menu pr�c�dent.\n'...
         '\n'...
         '\n'...
         'ENTREE pour continuer\n\n']);
     input('','s');     
end


%--------------------------------------------------------------------------
function aide_confirm
fprintf(['\n'...  
         '            Vous avez entr� une clef d''un dossier de simulation existant.\n'... 
         '            Si vous confirmez votre choix (r�ponse ''1''), ce dossier\n'...
         '            sera effac� et vous perdrez tout son contenu \n'...
         '            (fichiers de sauvegardes, graphiques et film).\n'...
         '\n'...
         '\n'...
         'ENTREE pour continuer\n\n']);
     input('','s');
end


%--------------------------------------------------------------------------
function aide_creersim
fprintf(['\n'...
    '     Les dossiers de simulation portent un nom de la forme simulforest_clef.\n'...
    '     Tapez ''clef'' pour cr�er la simulation simulforest_clef.\n'...
    '     ou ''q'' pour revenir au menu pr�c�dent.\n'...
    '\n'...
    '\n'...
    'ENTREE pour continuer\n\n']);
input('','s');
end

     
%--------------------------------------------------------------------------
function aide_askmodif(numsim,new)
if new
    fprintf([ '\n'...
                 '    Un fichier de param�tres a �t� cr�e pour le dossier simulforest_' numsim '\n'...
                 '    � partir des param�tres par d�faut (ceux enregistr�s dans ../forest/initforest.m).\n'])
    
    cas=['             Le fichier de simulation simulforest_' numsim ' sera effac�.\n'...
      '             Vous perdrez donc toutes les modifications de param�tres que vous\n'...
      '             venez d''effectuer.\n'];
    cas2=[];
    cas3='.\n';
else
    cas=['             Vous perdrez toutes les modifications de param�tres que vous\n'...
         '             venez d''effectuer.\n'];
    cas2=' pour la suite de la simulation';
    cas3=['\n'...
          '             dans la derni�re colonne.\n'];
end

fprintf(['\n'...
         '        1. Modifier un param�tre : \n'...
         '             Ce menu permet de modifier des param�tres' cas2 '.\n'...
         '        2. Lancer la simulation (avec les param�tres actuels)\n'...
         '             Ce menu lance le simulateur � partir des param�tres affich�s' cas3 ...
         '        q. Quitter (annule toutes les modifications de param�tres)\n'...
          cas '\n'...
         '\n'...
         '\n'...
         'ENTREE pour continuer\n\n']);
     input('','s');
     
end


%--------------------------------------------------------------------------
function aide_modifparamcorrect(numsim,new)

if new
 
 cas=['             Le fichier de simulation simulforest_' numsim ' sera effac�.\n'...
      '             Vous perdrez donc toutes les modifications de param�tres que vous\n'...
      '             venez d''effectuer.\n'];
 
else

  cas=['             Vous perdrez toutes les modifications de param�tres que vous\n'...
       '             venez d''effectuer.\n'];
  
end

fprintf(['\n'...
         '      Vous vous trouvez dans ce menu car les param�tres que vous avez entr�s ne sont pas conformes.\n'...
         '      Les raisons de cette non-conformit� vous ont �t� donn�es.\n\n'...
         '        1. Modifier un param�tre : \n'...
         '             Ce menu permet de modifier les param�tres notamment afin de les rendre conformes.\n'...
         '        2. Afficher les param�tres \n'...
         '             Permet d''afficher � nouveau  les param�tres enregistr�s\n'...
         '        q. Quitter (annule toutes les modifications de param�tres)\n'...
          cas '\n'...
         '\n'...
         '\n'...
         'ENTREE pour continuer\n\n']);
     input('','s');
     
end


%--------------------------------------------------------------------------
function aide_finsim(numsim) 
    fprintf(['\n'...
         '      Vous venez de terminer la simulation simulforest_' numsim '\n'...
         '      Vous pouvez : \n\n'...
         '        1. Continuer � travailler sur cette simulation  \n\n'...
         '        2. Retourner au menu principal\n'...
         '\n'...
         '\n'...
         'ENTREE pour continuer\n\n']);
     input('','s');
end


%--------------------------------------------------------------------------
function aide_analysesim(numsim)
fprintf(['\n\n                   Vous �tes actuellement dans le menu de travail de la simulation simulforest_' numsim '\n'...
         '                     qui a d�j� �t� r�alis�e. Vous avez le choix entre les menus suivant : \n'...
         '                     1. Interface graphique de sortie de simulforest_' numsim '\n'... 
         '                        Ce menu lance une interface graphique � partir de laquelle vous pourrez\n'...
         '                        a. afficher l''�tat de la for�t � n''importe quel moment\n'...
         '                        b. obtenir des courbes r�sumant la simulation\n'...
         '                        c. fabriquer le film de la simulation\n'...
         '                        d. cr�er des graphiques\n'...
         '                     2. Continuer simulforest_' numsim ':\n'...
         '                        Ce menu permet de continuer la simulation en changeant �ventuellement les param�tres\n'...
         '                     3. Afficher les param�tres utilis�s pour simulforest_' numsim '\n'...
         '\n'...
         '\n'...
         'ENTREE pour continuer\n\n']);
          input('','s'); 
end
     
     
    
