function initforest(numsim)

warning('off','MATLAB:MKDIR:DirectoryExists');
mkdir(['simulforest_' num2str(numsim)]);
mkdir(['simulforest_' num2str(numsim) '/save']);
mkdir(['simulforest_' num2str(numsim) '/fig']);
mkdir(['simulforest_' num2str(numsim) '/movie']);
warning('on','MATLAB:MKDIR:DirectoryExists');
Rm=0.1;             %1.taille min d'un arbre
RM=20;              %2.taille max d'un arbre
rc=0.25;            %3.taux de croissance par unit� de temps
taillep=6;          %4.taille minimum pour produire des graines
gamma=0.9;          %5.taux de production de graines par unit� de temps par unit� de taille
sigdis=12;          %6.Ecart type du noyau de dispersion (gaussien)
mu=0.001;           %7.mort naturelle
alpha=0.5;          %8.taux de mort par comp�tition

% Coordonn�es des coins de la for�t
Xm=0;               %9.Abscisse  du coin inf�rieur gauche
XM=100;             %10.Abscisse du coin sup�rieur droit
Ym=0;               %11.Ordonn�e du coin inf�rieur gauche
YM=100;             %12.Ordonn�e du coin sup�rieur droit

Size=100;           %13.Nombre initial d'individus (n�cessaire pour for�t non plant�e)
deltat=1/12;        %14.pas de temps des mises � jour des tailles
deltasave=1/12;     %15.pas de temps de sauvegarde
Niter=1e5;          %16.nombre d'it�rations (sonneries d'horloge)
CompteurMax=10000;  %17.Nombre d'arbres maximum (gestion de m�moire, nombre maximum d'arbres que peut contenir une for�t)

Tps=0;              %Date du d�but de la simulation
numfich=0;          %Indice du premier fichier de sauvegarde
param=[Rm,RM,rc,taillep,gamma,sigdis,mu,alpha,Xm,XM,Ym,YM,Size,deltat,deltasave,Niter,CompteurMax,Tps,Tps,numfich,numfich];

save(['simulforest_' numsim '/paramsim.mat'],'param');


       
end